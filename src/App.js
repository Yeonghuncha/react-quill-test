import React, { Component } from 'react';
import ReactQuill from 'react-quill'; // ES6
import { Modal, Button } from 'semantic-ui-react'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = { text: '', self: this}
    this.handleChange = this.handleChange.bind(this)
    this.imageHandler = this.imageHandler.bind(this);

    this.reactQuillRef = null; // ReactQuill component
    this.quillRef = null;      // Quill instance

    this.modules = {
      toolbar: {
        container:  [['bold', 'italic', 'underline', 'blockquote'],
              [{'list': 'ordered'}, {'list': 'bullet'}],
              ['image'],
        ],
        handlers: {
           'image': this.imageHandler
        }
      }
    }
  }

  imageHandler() {
    
    // 이미지 파일 선택 창 열기
    const input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');
    input.click();
    
    // 이미지 파일 선택 했을 때
    input.onchange = () => {
      const file = input.files[0];

      // 1. redux를 통해 이미지를 s3에 업로드하고 키값을 받아오는 메서드 작성하기
      
      
      // 2. editor에 키값이 부여된 img태그 삽입하기
      this.insertImg(); 

    };
  }

  handleChange(value) {
    this.setState({ text: value });
    this.refs.test.innerHTML = this.state.text;
  }
  
  insertImg = () => {
    // quillRef로 editor에 접근 가능 하도록 지정
    if (typeof this.reactQuillRef.getEditor !== 'function') return;
    this.quillRef = this.reactQuillRef.getEditor();

    var range = this.quillRef.getSelection();
    let position = range ? range.index : 0;
    this.quillRef.insertEmbed(position, 'image','https://images.pexels.com/photos/47480/pexels-photo-47480.jpeg?auto=compress&cs=tinysrgb&h=350');
  }

  render() {
    return (
      <div className="App">
      <Modal trigger={<Button>Show Quill</Button>}>
        <Modal.Header>Quill Editor</Modal.Header>
        <Modal.Content image>
          <ReactQuill value={this.state.text}
                      onChange={this.handleChange}
                      modules={this.modules}
                      formats={App.formats}
                      ref={(el) => {this.reactQuillRef = el}}
                      style={{width:"100%", minHeight:"500px"}}
          />
        </Modal.Content>
      </Modal>

      <br/><br/>
      <div contentEditable='true' ref='test'></div>
      {this.state.text}
      </div>

    );
  }
}
App.formats = [
  'header', 'font', 'size',
  'bold', 'italic', 'underline', 'strike', 'blockquote',
  'list', 'bullet', 'indent',
  'link', 'image', 'video'
]
export default App;
